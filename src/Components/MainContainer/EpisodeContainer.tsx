import React, { useState, useEffect } from "react";
import {
    Grid,
    Typography,
    Container,
    List, ListItem, Divider, Avatar, ListItemAvatar, ListItemText, Link
} from "@material-ui/core";

import useStyles from "../Styles";
import * as ApiManager from "../../Api/ApiManager";


export const EpisodeContainer = () => {
    const styles = useStyles();

    const [data, setData] = useState([]);

    useEffect(() => {
        async function getData() {
            const result: any = await ApiManager.getData("episode");
            setData(result);
        }
        getData();
    }, []);



    return (
        <Container className={styles.cardGrid} maxWidth="md">
            <Typography gutterBottom variant="h5" component="h2" className={styles.title}>
                Episode</Typography>
            <Link href="/DetailEpisode" className={styles.title}>See more...</Link>
            <Grid container className={styles.listGrid} spacing={4}>
                {data.map((card: any) => (
                    <List className={styles.listStyles}>
                        <ListItem alignItems="flex-start">
                            <ListItemAvatar>
                                <Avatar alt="R&M" src="../../Assets/avatar.jpg" />
                            </ListItemAvatar>
                            <ListItemText
                                primary={card.name}
                                secondary={
                                    <React.Fragment>
                                        <Typography
                                            component="span"
                                            variant="body2"
                                            className={styles.inline}
                                            color="textPrimary"
                                        >
                                            {card.air_date}
                                        </Typography>
                                        <Typography
                                            color="textPrimary"
                                        >
                                            {card.episode}
                                        </Typography>

                                    </React.Fragment>
                                }
                            />
                        </ListItem>
                        <Divider variant="inset" component="li" />

                    </List>
                ))}
            </Grid>
        </Container>
    );
};

export default EpisodeContainer;
