import React from 'react';
import { Link, Grid } from '@material-ui/core'
import CssBaseline from "@material-ui/core/CssBaseline";
import Appbar from '../../Components/Appbar/Appbar';
import Banner from '../../Components/Banner/Banner';
import Footer from '../../Components/Footer/Footer';
import CharacterContainer from '../../Components/MainContainer/CharacterContainer';
import EpisodeContainer from '../../Components/MainContainer/EpisodeContainer';
import LocationContainer from '../../Components/MainContainer/LocationContainer';

const Home = () => {
    return (
        <React.Fragment>
            <CssBaseline />
            <Appbar />
            <Banner />
            <main>
                <Grid container>
                    <Grid item xs><EpisodeContainer /></Grid>
                    <Grid item xs sm={6}><CharacterContainer /></Grid>
                    <Grid item xs><LocationContainer /></Grid>
                </Grid>
            </main>
            <Footer />


        </React.Fragment>

    )
}

export default Home;