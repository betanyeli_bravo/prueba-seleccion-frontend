import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import SignInScreen from '../Screens/SignInScreen/SignInScreen';
import RegisterScreen from '../Screens/RegisterScreen/RegisterScreen';
import Home from '../Screens/Home/Home';
import DetailCharacter from '../Screens/DetailCharacter/DetailCharacter';
import DetailEpisode from '../Screens/DetailEpisode/DetailEpisode';
import DetailLocation from '../Screens/DetailLocation/DetailLocation';

const PrivateRoute = ({ component, isAuthenticated, ...rest }: any) => {
    const routeComponent = (props: any) => (
        isAuthenticated
            ? React.createElement(component, props)
            : <Redirect to={{ pathname: '/Login' }} />
    );
    return <Route {...rest} render={routeComponent} />;
};



const Routes = () => {
    const [isAuth, setIsAuth] = useState(false)
    return (
        <Router>
            <Switch>
            <Route exact path="/">
                    <SignInScreen />
                </Route>
                <Route exact path="/Login">
                    <SignInScreen />
                </Route>
                <Route exact path="/Register">
                    <RegisterScreen />
                </Route>
                <PrivateRoute path="/Home">
                    <Home />
                </PrivateRoute>
                <PrivateRoute path="/DetailCharacter">
                    <DetailCharacter />
                </PrivateRoute>
                <PrivateRoute path="/DetailLocation">
                    <DetailLocation />
                </PrivateRoute>
                <PrivateRoute path="/DetailEpisode">
                    <DetailEpisode />
                </PrivateRoute>
            </Switch>
        </Router>
    )

}

export default Routes;