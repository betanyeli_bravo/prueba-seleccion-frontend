/* eslint-disable */
import axios from 'axios';


const loginEndpoint: any = process.env.REACT_APP_API_LOGIN_ENDPOINT;
const registerEndpoint: any = process.env.REACT_APP_API_REGISTER_ENDPOINT;
const apiEndpoint: any = process.env.REACT_APP_API_ENDPOINT;



export default class Auth {
    constructor(nickname: string, email: string, password: string) {
        nickname = '',
            email = '',
            password = ''
    }
    static async login(email: string, password: string) {
        try {
            let result: any = await axios.post(loginEndpoint, {"email": email, "password": password})
            
            console.log("result", result)
            return result
        } catch (error) {
            console.log("error", error)
            return "user not found"
        }

    }
    static async register (nickname: string, email: string, password: string){
        try {
            let result: any = await axios.post(registerEndpoint, {"nickname": nickname, "email": email, "password": password})
            
            console.log("result", result)
            return result
        } catch (error) {
            console.log("error", error)
            return "user not found"
        }
    }
    static signOut = () => {
        try {
            "haz algo"
        } catch (error) {
            "haz algo tmb"
        }
    }
}

export const getData = async (query: string) => {
    try {
        const result = await axios.get(`${apiEndpoint}/${query}`)
        console.log(result.data.results)
        return result.data.results
    } catch (error) {
        console.log(error)
    }
   

}