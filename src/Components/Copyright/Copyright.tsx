import React from 'react';
import {Typography, Link} from '@material-ui/core'

export default function Copyright() {
    return (
      <Typography variant="body2" color="textSecondary" align="center">
        {'Copyright © '}
        <Link color="inherit" href="https://www.netflix.com/cl/title/80014749">
          Rick & Morty Inc.
        </Link>{' '}
        {new Date().getFullYear()}
        {'.'}
      </Typography>
    );
  }