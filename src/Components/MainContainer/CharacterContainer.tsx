import React, { useState, useEffect } from "react";
import {
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
  Typography,
  Container,
  IconButton,
  Tooltip,
  Link
} from "@material-ui/core";
import FavoriteIcon from "@material-ui/icons/Favorite";
import useStyles from "../Styles";
import * as ApiManager from "../../Api/ApiManager";

export const CharacterContainer = () => {
  const styles = useStyles();

  const [data, setData] = useState([]);

  useEffect(() => {
    async function getData() {
      const result: any = await ApiManager.getData("character");
      setData(result);
    }
    getData();
  }, []);



  return (
    <Container className={styles.cardGrid} maxWidth="md">
      <Typography gutterBottom variant="h5" component="h2" className={styles.title}>
        Characters</Typography>
        <Link href="/DetailCharacter" className={styles.title}>See more...</Link>
      <Grid container spacing={4}>
        {data.map((card: any) => (
          <Grid item key={card.id} xs={12} sm={12} md={6}>
            <Card className={styles.card}>
              <CardMedia
                className={styles.cardMedia}
                image={card.image}
                title="Image title"
              />
              <CardContent className={styles.cardContent}>
                <Typography gutterBottom variant="h5" component="h2">
                  {card.name}
                </Typography>
                <Typography>
                  {card.status}
                </Typography>
                <Typography
                  component="span"
                  variant="body2"
                  className={styles.inline}
                  color="textPrimary"
                >
                  {card.species} - {card.gender}
                </Typography>
              </CardContent>
              <CardActions>
                <Tooltip title="Add to favs!" aria-label="Favs">
                  <IconButton color="secondary">
                    <FavoriteIcon />
                  </IconButton>
                </Tooltip>


              </CardActions>
            </Card>
          </Grid>
        ))}
      </Grid>
    </Container>
  );
};

export default CharacterContainer;
