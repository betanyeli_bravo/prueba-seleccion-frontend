import React from "react";
import useStyles from "../Styles";
import {Typography} from '@material-ui/core';
import Copyright from '../Copyright/Copyright'

export const Footer = () => {
  const styles = useStyles();
  return (
    <footer className={styles.footer}>
      <Typography variant="h6" align="center" gutterBottom>
        Developed with Love, by Beta
      </Typography>
      <Copyright />
    </footer>
  );
};

export default Footer;
