# Rick and Morty App!

Deploy: [https://dmqe50qm28ynp.cloudfront.net](https://dmqe50qm28ynp.cloudfront.net)
  
**Sprint duration:** :calendar:

Two days. 16-08 to 18-08

**Sprint goal**: :bar_chart:

Using the Reqres and Rick and Morty API,  create a ReactApp that allows you to Login and register, and show Characters, Episodes and Locations.

**Kanban board** : :chart_with_downwards_trend:
You can review the pending requirements in the "TO-DO" column
[https://trello.com/b/r1S4mIes/rick-and-morty](https://trello.com/b/r1S4mIes/rick-and-morty)

  

### Let's start!  :smiley:

- Clone this repository.

- Go to *prueba-seleccion-frontend* folder

- Add an .env file in root folder.

- Run in terminal `npm install`

- Run in terminal `npm start`

- Happy hacking! <3

  

## Functional requirements

- The app must allow login with username and password (simulate with reqres)
- The app must allow SIGN UP (simulate with reqres)
- Characters, locations and episodes must be loaded (load from Rick & Morty api). 
- It must have a text box which allows filtering the lists based on the name of the elements. 
- Show the image and detail of a specific character. The character selects from the list in Home.
- Show the image and detail of a specific episode. The episode selects from the list in Home.
- Show the image and detail of a specific location. The location selects from the list in Home.
- If any image does not exist, the placeholder service can be used to replace it.

## Non functional requirements

- Responsive Web App.
- Enviroment variables must be hidden,
- Protected routes

  
## Tech Stack

### Frontend:

- React Js with Hooks State management

- TypeScript

- MaterialUI (UI CSS Framework)

- Axios (Http request)

- Async Functions & Promises

- Reqres Api

- Rick and Morty Api.

- Typewriter effect.

### Backend:
- Amplify AWS CloudFront & S3

### :scroll: low fidelity prototype: :scroll:
-Pending... :construction:

### :rainbow: color palette :rainbow:
![palette_color](src/Assets/palette.PNG)


Develop with love by Beta :heart_eyes: