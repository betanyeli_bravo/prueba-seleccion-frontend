/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Avatar, Button, CssBaseline, TextField, FormControlLabel, Checkbox, Link, Grid, Box, Typography, Container } from '@material-ui/core';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Swal from 'sweetalert2';
import useStyles from './RegisterScreenStyles';
import Copyright from '../../Components/Copyright/Copyright';
import Auth from '../../Api/ApiManager';
import { Redirect } from 'react-router-dom';


export default function SignUp() {
  const classes = useStyles();
  const [isAuth, setIsAuth] = useState(false);
  const [nickname, setNickname] = useState('');
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  useEffect(() => {
    if (username.trim() && password.trim() && nickname.trim()) {
      setIsButtonDisabled(false);
    } else {
      setIsButtonDisabled(true);
    }
  }, [username, password, nickname]);

  const Register = async () => {
    const result = await Auth.register(nickname, username, password)
    if (result.data !== undefined) {
      Swal.fire({
        icon: 'success',
        title: `Hi, ${result.data.nickname}`,
        text: 'Welcome to Rick & Morty Inc!',
        footer: '<a href="/Home">Go to home</a>'
      })
      setIsAuth(true)

    } else {
      Swal.fire({
        icon: 'error',
        title: 'Oops :( ...',
        text: 'Try later...',
        footer: '<a href="/Register">Sign up here!</a>'
      })
      setIsAuth(false)
    }


  };

  return (
    isAuth ? <Redirect to="/Home" /> : <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
          Sign up
        </Typography>
        <form className={classes.form} noValidate>
          <Grid container spacing={2}>
            <Grid item xs={12}>
              <TextField
                autoComplete="fname"
                name="nickName"
                variant="outlined"
                required
                fullWidth
                id="nickName"
                label="NickName"
                autoFocus
                onChange={(e) => setNickname(e.target.value)}
              />
            </Grid>

            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                onChange={(e) => setUsername(e.target.value)}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
              />
            </Grid>

          </Grid>
          <Link>
          </Link>

          <Button
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            disabled={isButtonDisabled}
            onClick={() => Register()}
          >
            Sign Up
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/Login" variant="body2">
                Already have an account? Sign in
              </Link>
            </Grid>
          </Grid>
        </form>
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>

  );
}


