/* eslint-disable */
import React, { useState, useEffect } from 'react';
import { Avatar, Button, CssBaseline, TextField, FormControlLabel, Checkbox, Link, Paper, Box, Grid, Typography } from '@material-ui/core';
import LiveTvIcon from '@material-ui/icons/LiveTv';
import Swal from 'sweetalert2';
import useStyles from './SignInScreenStyles';
import Auth from '../../Api/ApiManager';
import Copyright from '../../Components/Copyright/Copyright';
import { Redirect } from 'react-router-dom';
//import Typewriter from 'typewriter-effect';

export default function SignInScreen() {
  const classes = useStyles();
  const [isAuth, setIsAuth] = useState(false)
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [isButtonDisabled, setIsButtonDisabled] = useState(true);
  useEffect(() => {
    if (username.trim() && password.trim()) {
      setIsButtonDisabled(false);
    } else {
      setIsButtonDisabled(true);
    }
  }, [username, password]);

  const Login = async () => {
    const result = await Auth.login(username, password)
    console.log("result login", result)
    //Reqres api login is not working, It should be result !== "user not found"
    result !== undefined ? setIsAuth(true) : Swal.fire({
      icon: 'error',
      title: 'Oops :( ...',
      text: 'Are you sure you are registered?',
      footer: '<a href="/Register">Sign up here!</a>'
    })
  };


  return (
    isAuth ? <Redirect to="/Home" /> :
      <Grid container component="main" className={classes.root}>
        <CssBaseline />
        <Grid item xs={false} sm={4} md={7} className={classes.image} />
        <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
          <div className={classes.paper}>
          {/* <Typewriter
                options={{
                  strings: ['Welcome to Rick and Morty App', 'Sign In and Enjoy It 😆'],
                  autoStart: true,
                  loop: true,
                }}
              /> */}
            <Avatar className={classes.avatar}>
              <LiveTvIcon />
            </Avatar>
            
            <Typography component="h1" variant="h5">
            Welcome to Rick and Morty App, Sign In and Enjoy It 😆
          </Typography>
            <form className={classes.form} noValidate>
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Email Address"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={(e) => setUsername(e.target.value)}
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Password"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange={(e) => setPassword(e.target.value)}
              />
              <FormControlLabel
                control={<Checkbox value="remember" color="primary" />}
                label="Remember me"
              />

              <Button

                fullWidth
                variant="contained"
                color="primary"
                className={classes.submit}
                onClick={() => Login()}
                disabled={isButtonDisabled}

              >
                Sign In
            </Button>


              <Grid container>

                <Grid item>
                  <Link href="/Register" variant="body2">
                    {"Don't have an account? Sign Up and enjoy it!"}
                  </Link>
                </Grid>
              </Grid>
              <Box mt={5}>
                <Copyright />
              </Box>
            </form>
          </div>
        </Grid>
      </Grid>
  );
}