import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
  background: {
    color: "#fff",
    backgroundColor: '#55a630',
  },
  icon: {
    marginRight: theme.spacing(2),
  },
  infoContent: {
    color: '#e6a7c1a6',
    padding: theme.spacing(8, 0, 6),
    backgroundImage: "url(https://cdn.hipwallpaper.com/i/61/36/MlFnUo.jpg)",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    width: '100%',
    height:'100vh',
    [theme.breakpoints.down('sm')]: {

    backgroundImage: "url(https://cdn.hipwallpaper.com/i/61/36/MlFnUo.jpg)",
    backgroundSize: "cover",
    backgroundRepeat: "no-repeat",
    backgroundPosition: "center",
    width: '100%',
    height:'auto'

    },
  },
  infoButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
  },
  cardMedia: {
    width: "100%",
    height: "250px",
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    backgroundColor: '#55a630',
    padding: theme.spacing(6),
  },
  listGrid: {
    paddingTop: theme.spacing(2),
  },
  listStyles: {
    width: '100%',
    maxWidth: '36ch',
    backgroundColor: theme.palette.background.paper,

  },
  inline: {
    display: 'inline',
  },
  title:{
    textAlign: 'center'
  },

}));

export default useStyles;
