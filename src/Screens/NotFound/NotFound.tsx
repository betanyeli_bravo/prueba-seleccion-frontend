import React from 'react';
import { Link, Container } from '@material-ui/core';
import { makeStyles } from "@material-ui/core/styles";


const useStyles = makeStyles((theme) => ({
  root: {
    color: "#fff",
    backgroundImage:"url(https://i.ibb.co/VSV8rpd/assets.png)",
    objectFit:'cover',
    backgroundSize:'cover',
    width:'100%',
    height:'100vh'
  },
 
}));


const NotFound = () => {
    const styles = useStyles();
    return (
        <React.Fragment>
            <Container className={styles.root}>
                <Link href="/Home" variant="body2">
                    Go to Home
      </Link>
            </Container>
        </React.Fragment>

    )
}

export default NotFound;