import React from "react";
import { AppBar, Toolbar, Typography } from "@material-ui/core";

import useStyles from "../Styles";

const Appbar = () => {
  const styles = useStyles();
  return (
    <AppBar position="relative" className={styles.background}>
      <Toolbar>
        <Typography variant="h6" color="inherit" noWrap>
        Be good Morty, be better than me!
        </Typography>
      </Toolbar>
    </AppBar>
  );
};

export default Appbar;
